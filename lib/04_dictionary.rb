class Dictionary
  attr_reader :entries, :keywords

  def initialize
    @entries = Hash.new(0)
  end

  def add(entry)
    @entries[entry] = nil if entry.is_a? String
    @entries[entry.keys[0]] = entry.values[0]  if entry.is_a? Hash
  end

  def keywords
    @keywords = @entries.keys.sort
  end

  def include?(keyword)
    @entries.has_key?(keyword)
  end

  def find(string)
    @entries.select {|entry| entry.include?(string)}
  end

  # Produce printable output like so: [keyword] 'definition'
  def printable
    self.keywords.map do |key|
      "[#{key}] \"#{@entries[key]}\""
    end.join("\n")
  end
end
