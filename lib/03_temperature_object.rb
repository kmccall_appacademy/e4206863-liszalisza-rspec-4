class Temperature
  attr_accessor :temp_input, :degrees

  def initialize(options = {})
    @temp_input = options.keys.first
    @degrees = options[@temp_input].to_f
  end

  def in_celsius
    if @temp_input == :c
      @degrees
    else
      (@degrees - 32) * 5/9
    end
  end

  def in_fahrenheit
    if @temp_input == :f
      @degrees
    else
      @degrees * 9/5 + 32
    end
  end

  # Factory methods. Class methods, not instance methods. Instance can be constructed directly by calling this method like so Temperature.from_celsius(50)
  def self.from_celsius(degrees)
    return Temperature.new(:c => degrees)
  end

  def self.from_fahrenheit(degrees)
    return Temperature.new(:f => degrees)
  end
end

# Celsius and Fahrenheit subclasses
class Celsius < Temperature
  def initialize(degrees)
    # The subclass chains to the super class's method and passes super(args) as arguments. Does the same as directly creating the instance variables with
    # @temp_input = :c
    # @degrees = degrees
    super(:c => degrees)
  end
end

class Fahrenheit < Temperature
  def initialize(degrees)
    super(:f => degrees)
  end
end
