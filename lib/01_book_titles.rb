class Book
  def title
    @title
  end

  def title=(name)
    @title = titleize(name)
  end

  def titleize(str)
    little_words = ["a", "an", "the", "and", "but", "or", "of", "in", "for", "nor", "on", "at", "to", "from", "by", "over"]

    str.split.map.with_index do |word, i|
      if little_words.include?(word) && i != 0
        word.downcase
      else
        word.capitalize
      end
    end.join(" ")
  end
end
