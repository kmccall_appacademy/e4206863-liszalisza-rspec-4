class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    hrs = @seconds / 3600
    mins = (@seconds % 3600) / 60
    secs = (@seconds % 60)
    
    @time_string = "#{padded(hrs)}:#{padded(mins)}:#{padded(secs)}"
  end

  def padded(num)
    num < 10 ? "0" + num.to_s : num.to_s
  end
end
